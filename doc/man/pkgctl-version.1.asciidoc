pkgctl-version(1)
=================

Name
----
pkgctl-version - Check and manage package versions against upstream


Synopsis
--------
pkgctl version [OPTIONS] [SUBCOMMAND]

Description
-----------

Commands related to package versions, including checks for outdated packages.

Uses linkman:nvchecker[1] and a `.nvchecker.toml` file located alongside the
PKGBUILD.

Configuration
-------------

The `.nvchecker.toml` file must contain a section that matches the
package's pkgbase. The pkgbase section within the `.nvchecker.toml` file
specifies the source and method for checking the latest version of the
corresponding package.

For detailed information on the various configuration options available for the
`.nvchecker.toml` file, refer to the configuration files section in
linkman:nvchecker[1]. This documentation provides insights into the possible
options that can be utilized to customize the version checking process.

To supply GitHub or GitLab tokens to nvchecker, a `keyfile.toml` should be
placed in the `$XDG_CONFIG_HOME`/nvchecker` directory. This keyfile is
used for providing the necessary authentication tokens required for
accessing the GitHub or GitLab API.

Options
-------

*-h, --help*::
	Show a help text

Subcommands
-----------

pkgctl version check::
	Compares local package versions against upstream

pkgctl version upgrade::
	Adjust the PKGBUILD to match the latest upstream version

See Also
--------

linkman:pkgctl-version-check[1]
linkman:pkgctl-version-upgrade[1]

include::include/footer.asciidoc[]
